﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class WorldRequestRouter {
        private static readonly Dictionary<RequestTypes, Type> requestMap = new Dictionary<RequestTypes, Type>() {
            { RequestTypes.AuthenticateUser, typeof(AuthenticateUserRequest) },
            { RequestTypes.Ping, typeof(PingRequest) },
            { RequestTypes.LoadCharacters, typeof(LoadCharactersRequest) },
            { RequestTypes.NextRoomInformation, typeof(NextRoomInformationRequest) },
            { RequestTypes.ChatPacket, typeof(ChatConsoleRequest) },
            { RequestTypes.BeginCharacterCreation, typeof(BeginCharacterCreationRequest) },
            { RequestTypes.EraseCharacter, typeof(EraseCharacterRequest) },
            { RequestTypes.MoveToNextScreen, typeof(MoveToNextScreenRequest) },
            { RequestTypes.FinishCreateCharacter, typeof(FinishCreateCharacterRequest) },
            { RequestTypes.InspectItem, typeof(InspectItemRequest) },
            { RequestTypes.CreateCharacterData, typeof(CreateCharacterDataRequest) },
            { RequestTypes.ExitCombatCloud, typeof(ExitCombatCloudRequest) },
            { RequestTypes.ShowShopkeeperScreen, typeof(ShowShopkeeperScreenRequest) },
            { RequestTypes.SellItems, typeof(SellItemsRequest) },
            { RequestTypes.SellItem, typeof(SellItemRequest) },
            { RequestTypes.DropGoldOrMana, typeof(DropGoldOrManaRequest) },
            { RequestTypes.PickUpGoldOrMana, typeof(PickUpGoldOrManaRequest) },
            { RequestTypes.SpellCasted, typeof(SpellCastedRequest) },
            { RequestTypes.DebuggingInfoFromLastCrash, typeof(DebuggingInformationRequest) },
            { RequestTypes.LoggedIntoLastRoom, typeof(LogInToLastRoomRequest) },
            { RequestTypes.ChangePassword, typeof(ChangePasswordRequest) },
            { RequestTypes.TeleportToHouseRequest, typeof(TeleportToHouseRequest) },
            { RequestTypes.EnterDoor, typeof(EnterDoorRequest) },
            { RequestTypes.NpcChatDialog, typeof(NpcChatDialogRequest) },
            { RequestTypes.ShowQuestScreen, typeof(ShowQuestScreenRequest) },
            { RequestTypes.ShowWhatsNew, typeof(ShowWhatsNewRequest) },
            { RequestTypes.CombatRound, typeof(CombatRoundRequest) },
            { RequestTypes.ShopBuy, typeof(ShopBuyRequest) },
            { RequestTypes.ShopSell, typeof(ShopSellRequest) },
            { RequestTypes.SellScreen, typeof(ShowSellScreenRequest) },
            { RequestTypes.CreateGossipChannel, typeof(CreateGossipChannelRequest) },
            { RequestTypes.LogIn, typeof(LogInRequest) },
            { RequestTypes.ShowMagicMail, typeof(ShowMagicMailRequest) },
            { RequestTypes.CreateMagicMail, typeof(CreateMagicMailRequest) },
            { RequestTypes.DeleteMagicMail, typeof(DeleteMagicMailRequest) },
            { RequestTypes.ReadMagicMail, typeof(ReadMagicMailRequest) },
            { RequestTypes.PickupItem, typeof(PickupItemRequest) },
            { RequestTypes.DropItem, typeof(DropItemRequest) },
            { RequestTypes.MoveItemToBag, typeof(MoveItemToBagRequest) },
            { RequestTypes.EquipItem, typeof(EquipItemRequest) },
            { RequestTypes.UnequipItem, typeof(UnEquipItemRequest) },
            { RequestTypes.OpenDoorOrChest, typeof(OpenDoorOrChestRequest) },
            { RequestTypes.CloseItem, typeof(CloseItemRequest) },
            { RequestTypes.EnterCombat, typeof(EnterCombatRequest) },
            { RequestTypes.UseItem, typeof(UseItemRequest) },
            { RequestTypes.GiveItem, typeof(GiveItemRequest) },
            { RequestTypes.UseInventoryItem, typeof(UseInventoryItemRequest) },
            { RequestTypes.Dye, typeof(DyeRequest) },
            { RequestTypes.LoggedInUserCount, typeof(LoggedInUserCountRequest) },
            { RequestTypes.PlayerOrMobMovement, typeof(PlayerOrMobMovementRequest) }
        };

        public static void HandleRequest(WorldClient client, byte[] packets, int packetId) {
            try {
                //temporary additional handling until all requests are mapped properly
                if (IsMappedRequest(packetId)) {
                    Type handlerType = requestMap[(RequestTypes)packetId];
                    BaseWorldRequest request = Activator.CreateInstance(handlerType, client, packets) as BaseWorldRequest;

                    request.SendResponse();
                }
                else {
                    //fall back to big case statement if no mapping exists yet
                    UnknownRequest.HandleRequest(client, packets, packetId);
                }
            }
            catch (Exception e) {
                Console.WriteLine("Unable to handle request with packet ID {0}, payload: {1}", packetId, packets);
            }
        }

        private static bool IsMappedRequest(int packetId) {
            return Enum.IsDefined(typeof(RequestTypes), packetId) && requestMap.ContainsKey((RequestTypes)packetId);
        }
    }
}
