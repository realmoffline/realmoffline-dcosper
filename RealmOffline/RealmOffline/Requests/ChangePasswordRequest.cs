﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ChangePasswordRequest: BaseWorldRequest {
        public ChangePasswordRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            //Console.WriteLine("Change Password {0}", BitConverter.ToString(pak));
            Reader.Close();
            PacketReader r = new PacketReader(Packets);
            r.ReadBytes(8);
            uint id = r.ReadUInt32();
            string oldp = r.ReadString16();
            string newp = r.ReadString16();
            r.Close();

            bool match = Client.GameAccount.CurrentCharacter.GameID == id;

            Console.WriteLine("Original Password {0} changed to {1}, the charids {2} match.", oldp, newp, match ? "do" : "do not");
            PacketWriter w = new PacketWriter(0x19);
            w.WriteUInt32(Client.GameAccount.AccountId);
            w.WriteUInt32(0x00);
            w.WriteUInt32(0x5D);
            w.WriteBytes(new byte[] { 0x7A, 0xDD, 0x08, 0x08 });
            byte[] a = w.ToArray();
            Client.Send(ref a);
        }
    }
}
