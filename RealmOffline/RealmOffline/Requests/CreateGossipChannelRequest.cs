﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class CreateGossipChannelRequest: BaseWorldRequest {
        public CreateGossipChannelRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // Creates a channel
            Character c = Client.GameAccount.CurrentCharacter;
            Reader.ReadBytes(8);
            ushort newChannelNum = Reader.ReadUInt16();
            ushort titleLength = Reader.ReadUInt16();

            string title = Encoding.ASCII.GetString(Reader.ReadBytes(titleLength)).CleanEncodedString();

            ushort topiclength = Reader.ReadUInt16();

            string topic = Encoding.ASCII.GetString(Reader.ReadBytes(topiclength)).CleanEncodedString();

            ushort passlength = Reader.ReadUInt16();
            string password = Encoding.ASCII.GetString(Reader.ReadBytes(passlength)).CleanEncodedString();
            Reader.Close();

            // So check to make sure this channel number isnt already in use, 
            // if its not, then make the channel and add it
            var channel = Channel.FromCode(c.Name, newChannelNum.ToString(), title, topic, password);
            // add the player to the chanel

            ScriptResolver.ImportedChannels.AddChannel(channel);
            channel.SendJoinPacket(Client.GameAccount);
            Client.GameAccount.CurrentChannel = channel;
        }
    }
}
