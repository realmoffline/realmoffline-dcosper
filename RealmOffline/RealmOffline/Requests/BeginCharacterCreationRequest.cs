﻿using RealmOffline.Accounts;
using RealmOffline.Managers;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RealmOffline.Requests {
    class BeginCharacterCreationRequest : BaseWorldRequest {
        public BeginCharacterCreationRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Tools.SetColor(ConsoleColor.Yellow);
            Console.WriteLine("Creation Packet  48: {0}", BitConverter.ToString(Packets));

            Tools.ResetColor();
            Reader.ReadBytes(4); // pck len
            Reader.ReadBytes(4); // Id
                                 // Ok need to make a new char here
                                 // Lets begin to create our new char
            Client.GameAccount.InCharacterCreationProcess = true;
            Client.GameAccount.CurrentCharacter = new Character();
            //var cBuilder = Client.GameAccount.CurrentCharacter;
            //  string fileLocation1 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CharCreate0x3010f3Client.txt");
            //  File.WriteAllText(fileLocation1, BitConverter.ToString(pak));


            ushort uNameLen = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
            string uName = Encoding.ASCII.GetString(Reader.ReadBytes(uNameLen));

            if (!MySqlManager.IsNameTaken(uName)) {
                uint sqlId = 0;

                ushort uTtitleLen = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
                string uTitle = Encoding.ASCII.GetString(Reader.ReadBytes(uTtitleLen));

                Client.GameAccount.CurrentCharacter.AddCharData(uName, uTitle, Reader.ReadByte(),
                    Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(),
                    Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(), Reader.ReadByte(),
                    Reader.ReadByte(), Reader.ReadByte());
                Reader.Close();

                switch (Client.GameAccount.CurrentCharacter.Class) {
                    case 0x00: // adv
                        {
                            Client.GameAccount.CurrentCharacter.CurrentHP = 45;
                            Client.GameAccount.CurrentCharacter.TotalHP = 45;
                            Console.WriteLine("Adv");
                        }
                        break;
                    case 0x01://warrior
                        {
                            Client.GameAccount.CurrentCharacter.CurrentHP = 60;
                            Client.GameAccount.CurrentCharacter.TotalHP = 60;
                            Console.WriteLine("War");
                        }
                        break;
                    case 0x02: //wiz
                        {
                            Client.GameAccount.CurrentCharacter.CurrentHP = 15;
                            Client.GameAccount.CurrentCharacter.TotalHP = 15;
                            Console.WriteLine("Wiz");
                        }
                        break;
                    case 0x03: //thief
                        {
                            Client.GameAccount.CurrentCharacter.CurrentHP = 25;
                            Client.GameAccount.CurrentCharacter.TotalHP = 25;
                            Console.WriteLine("Thief");
                        }
                        break;
                    default:
                        Console.WriteLine("Unknown Class {0}.", Client.GameAccount.CurrentCharacter.Class);
                        break;
                }
                Client.GameAccount.CurrentCharacter.Gold = 10000;
                Client.GameAccount.CurrentCharacter.Mana = 10000;
                Client.GameAccount.CurrentCharacter.Level = 1;
                Client.GameAccount.CurrentCharacter.BuildPoints = 5;
                bool created = MySqlManager.CreateNewCharacter(Client.GameAccount.CurrentCharacter, Client.GameAccount.SqlId, out sqlId);


                //    Console.WriteLine("Created new char {0} {1}", uName, created);
                //  cBuilder.SqlCharId = sqlId;
                // The character is finished up to this point
                // we have 2 more sections to add next is 59
                //  Character c = MySqlManager.GetCharacter(sqlId);
                //  if (c == null) Console.WriteLine("cant get new char");
                //Client.GameAccount.CurrentCharacter = c;
                byte[] reply = Client.GameAccount.CurrentCharacter.CreateNewChar48(Client.GameAccount.AccountId);//Client.GameAccount.GenerateCreateCharPacket();
                string fileLocation = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "MyPacket25(NewChar).txt");
                File.WriteAllText(fileLocation, BitConverter.ToString(reply));
                Console.WriteLine("Sent Char Create 1");
                Client.Send(ref reply);
            }
            else {
                byte[] no = { 0x14, 0x00, 0x00, 0x00, 0x1A, 0x00, 0x00, 0x00, 0x23, 0x0A, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x00, 0x00, 0x00, 0x11, 0x27, 0x00, 0x00, 0x83, 0xCF, 0x49, 0xC4 };
                Client.Send(ref no);
            }
        }
    }
}
