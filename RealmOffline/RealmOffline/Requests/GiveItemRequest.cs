﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class GiveItemRequest: BaseWorldRequest {
        public GiveItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // looks like it sends the same packet 42 twice (Confirmed)
            // then sends  the packet 25
            // then sends a packet 42 looks like a update, with the id of person we gave too
            Reader.ReadBytes(8);

            uint givee = Reader.ReadUInt32();
            uint item = Reader.ReadUInt32();
            Reader.Close();
            Console.WriteLine("Give packet {0}", BitConverter.ToString(Packets));
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0xA4);
            w25.WriteBytes(new byte[] { 0x65, 0x25, 0x05, 0x08 });
            byte[] p25 = w25.ToArray();
        }
    }
}
