﻿using RealmOffline.Accounts;
using RealmOffline.Managers;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RealmOffline.Requests {
    class LoadCharactersRequest : BaseWorldRequest {
        public LoadCharactersRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // This packet gives back loaded chars server packet id is 18
            // If we have chars we send packet 18 formatted with chars, if not
            // we send a blank one
            // Seems to be in chunks of 21 bytes the 21st byte is FF i assume to mark the ending
            // of the array

            if (ServerGlobals.LoadFromSql) {
                Client.GameAccount.Characters = MySqlManager.LoadCharactersFromAccount(Client.GameAccount.SqlId);


                // Always list chars alphabetically
                Client.GameAccount.Characters = Client.GameAccount.Characters.OrderBy(c => c.Name).ToList<Character>();
                // Set the chars into our slots


                CharacterPacket cp = new CharacterPacket(Client.GameAccount, Client.GameAccount.Characters.ToArray());

                byte[] reply = cp.Packet0x12(Client.GameAccount);//cp.GetPacket(cli.GameAccount.AccountId);
                Client.Send(ref reply);
                //  Console.WriteLine("The packet is {0} we think its {1}", reply.LongLength -8, BitConverter.ToUInt32(reply, 0));
            }
            else {
                Console.WriteLine("Loading packet 18 from CharDataFile.txt");
                // Reads the charfile from CharDataFile.txt
                string fileLocation = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CharDataFile.txt");
                byte[] fake0 = RealmPacketIO.GetByteArrayFromFile(fileLocation);
                Client.Send(ref fake0);
            }
        }
    }
}
