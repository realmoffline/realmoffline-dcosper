﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class LogInRequest: BaseWorldRequest {
        public LogInRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            // Login to game ?
            Reader.ReadBytes(4); // len
            Reader.ReadBytes(4); // id
            uint charID = BitConverter.ToUInt32(Reader.ReadBytes(4), 0);
            Character c = Client.GameAccount.GetCharFromGameID(charID);
            if (c == null) Console.WriteLine("Null Char");
            Client.GameAccount.CurrentCharacter = c;
            c.GameMail = new MagicMail(Client.GameAccount);
            if (c.GameMail == null) Console.WriteLine("Null GameMail");

            byte[] reply = c.CreateLogin130(Client.GameAccount.AccountId);
            Client.Send(ref reply);

            // check busy
            PacketWriter busy = new PacketWriter(0x25);
            busy.WriteString("/unbusy");
            byte[] u = busy.ToArray();
            //Client.Send(ref u);

            // Join last gossip chan
            PacketWriter join = new PacketWriter(0x25);
            join.WriteString("/join 4");
            byte[] j = join.ToArray();
            // Client.Send(ref j);


            // Send Magic Mail
            WorldRequestRouter.HandleRequest(Client, new byte[12], 0x87);
            //Console.WriteLine("Logging in {0}", BitConverter.ToString(pak));
            // Console.WriteLine("Sent 130");
        }
    }
}
