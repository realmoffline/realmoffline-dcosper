﻿using RealmOffline.Base;
using RealmOffline.Managers;
using RealmOffline.Packets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests
{
    public abstract class BaseRequest: IRequest
    {
        protected byte[] Packets { get; }
        protected PacketWriter PacketWriter { get; }
        private XMLManager xmlManager;
        protected XMLManager XMLManager {
            get {
                if (xmlManager == null) {
                    xmlManager = new XMLManager();
                }

                return xmlManager;
            }
        }

        public BaseRequest(byte[] packets){
            PacketWriter = new PacketWriter();
            Packets = packets;

            Console.WriteLine("Initializing request {0}", this.GetType().ToString());
        }

        public abstract byte[] GetResponse();
    }
}
