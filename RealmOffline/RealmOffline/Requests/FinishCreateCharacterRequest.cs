﻿using RealmOffline.Accounts;
using RealmOffline.Managers;
using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace RealmOffline.Requests {
    class FinishCreateCharacterRequest: BaseWorldRequest {
        public FinishCreateCharacterRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine("Final Char Create");
            // not sent atm fix me.
            //  Console.WriteLine("Reply from creation 56: {0}", BitConverter.ToString(pak));
            // Final part of create char, we finalize stuff here
            Reader.ReadBytes(4); // pck len
            Reader.ReadBytes(4); // pack id
            Reader.ReadBytes(4);  // Not sure what this is
            ushort bioLen = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
            byte[] bTitle = Reader.ReadBytes(bioLen);
            Reader.Close();
            string fileLocation1 = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "CharCreate0x3830f3Client.txt");
            File.WriteAllText(fileLocation1, BitConverter.ToString(Packets));

            Character c = Client.GameAccount.CurrentCharacter;
            //  Console.WriteLine("New chars id is {0}", c.CharId);
            if (c == null) { Console.WriteLine("Char should not be null in 56"); }
            c.AddDescription(bTitle);
            // Now we need to generate default skills

            //  Console.WriteLine("Packet 56 from client answered");
            PacketWriter w = new PacketWriter(0x19);
            w.WriteUInt32(Client.GameAccount.AccountId);
            w.WriteUInt32(0);
            w.WriteUInt32(0x38);
            //   w.WriteBytes(new byte[] { 0x98, 0xC3, 0x71, 0x0F });
            byte[] reply = w.ToArray();
            //c.CreateNewChar56(Client.GameAccount.AccountId);

            // This brand new char is completed
            // we need to add this in the correct slot in the acounts chars

            // Now we need to make sure mysql is populated correctly
            // assign more vars
            c.Gold = 1000000;//750;
            bool g = MySqlManager.TryUpdateCharValue("Gold", 1000000, c.SqlCharId);
            c.Mana = 1000000;//500;
            bool m = MySqlManager.TryUpdateCharValue("Mana", 1000000, c.SqlCharId);
            c.Level = 1;
            bool l = MySqlManager.TryUpdateCharValue("Level", 1, c.SqlCharId);
            c.CurrentHP = 30;
            bool ch = MySqlManager.TryUpdateCharValue("CurrentHP", 30, c.SqlCharId);
            c.TotalHP = 30;
            bool th = MySqlManager.TryUpdateCharValue("TotalHP", 30, c.SqlCharId);
            c.BuildPoints = 54;
            bool b = MySqlManager.TryUpdateCharValue("BuildPoints", 1, c.SqlCharId);
            Client.Send(ref reply);
            if (Client.GameAccount.AddCharacter(c)) {
                Console.WriteLine("Good Char add chars now at {0}", Client.GameAccount.NumberOfValidCharacters);
            }
            Console.WriteLine("Sent Char create final");
        }
    }
}
