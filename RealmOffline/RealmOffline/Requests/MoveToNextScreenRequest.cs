﻿using RealmOffline.Accounts;
using RealmOffline.Core;
using RealmOffline.Core.Rooms;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class MoveToNextScreenRequest: BaseWorldRequest {
        public MoveToNextScreenRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Character c = Client.GameAccount.CurrentCharacter;
            Reader.ReadBytes(4);
            Reader.ReadBytes(4);
            ushort exitNum = BitConverter.ToUInt16(Reader.ReadBytes(2), 0);
            Reader.Close();

            // this is the exit we leaving by
            RealmOffline.Core.Rooms.Room.RoomExit e = Client.GameAccount.CurrentCharacter.Location.CurrentRoom.GetRoomExitByValue((byte)exitNum);
            uint newRoomNum = Client.GameAccount.CurrentCharacter.Location.CurrentRoom.GetNextRoomByExit(e);
            Room foundRoom = null;
            bool found = ScriptResolver.ImportedRooms.TryGetRoom(newRoomNum, out foundRoom);
            if (!found) found = ScriptResolver.ImportedRooms.TryGetRoom(5043, out foundRoom);

            Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RemoveEntity(Client.GameAccount.CurrentCharacter, Client.GameAccount);
            foundRoom.AddEntity(Client.GameAccount.CurrentCharacter, Client.GameAccount);
            Client.GameAccount.CurrentCharacter.Location.CurrentRoom = foundRoom;
            byte[] room = foundRoom.GetRoomPacket(Client.GameAccount.AccountId);
            Client.Send(ref room);
        }
    }
}
