﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class UseInventoryItemRequest: BaseWorldRequest {
        public UseInventoryItemRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            Console.WriteLine("Use a item or a book ? {0}", BitConverter.ToString(Packets));
            Reader.ReadBytes(8);
            uint itemid = Reader.ReadUInt32();
            Reader.Close();
            Console.WriteLine("Wanting to use inventory item {0}", itemid);
            PacketWriter w25 = new PacketWriter(0x19);
            w25.WriteUInt32(Client.GameAccount.AccountId);
            w25.WriteUInt32(0x00);
            w25.WriteUInt32(0xA5);
            w25.WriteBytes(new byte[] { 0x88, 0x86, 0x4A, 0x0C });
            byte[] p25 = w25.ToArray();
            Client.Send(ref p25);
            // this has to destroy a item
            PacketWriter w42 = new PacketWriter(0x2A);
            w42.WriteUInt32(Client.GameAccount.AccountId);
            w42.WriteUInt32(0x00);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.Location.CurrentRoom.RoomID);
            w42.WriteByte(0x32);
            w42.WriteUInt32(Client.GameAccount.CurrentCharacter.GameID);
            w42.WriteUInt32(itemid);
            w42.WriteShort(0x1C);
            w42.WriteByte(0xFF);
            w42.WriteByte(0xFF);
            w42.WriteByte(0x21);
            w42.WriteByte(0xFF);
            byte[] p42 = w42.ToArray();
            Client.Send(ref p42);
        }
    }
}
