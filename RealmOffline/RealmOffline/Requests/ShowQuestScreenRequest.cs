﻿using RealmOffline.Packets;
using RealmOffline.Tcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RealmOffline.Requests {
    class ShowQuestScreenRequest: BaseWorldRequest {
        public ShowQuestScreenRequest(WorldClient client, byte[] packets) : base(client, packets) { }

        public override void SendResponse() {
            PacketWriter w = new PacketWriter(25);
            w.WriteUInt32(Client.GameAccount.AccountId);
            w.WriteInt32(0);
            w.WriteInt32(0x6E);
            w.WriteInt32(0); // No Quests
            w.WriteInt32(0);

            byte[] reply = w.ToArray();
            Client.Send(ref reply);
        }
    }
}
