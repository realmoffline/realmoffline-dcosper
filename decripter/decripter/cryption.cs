﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace decripter
{
    // This is XOR
        class cryption
        {
        // Changing multiplier ?
            public int muliplier = 0;
        // Static multiplier ?
            int m2 = 0x180EF;
        // Need to figure out how the crc is generated in original packets
            int crc;
        // Why does he switch back and forth between a int array and a byte array ?
            public byte[] data;
            int[] worddata;
        //-----------------------------
            private bool skip = true;

            public cryption(byte[] ip)
            {
                data = ip;
            }

            public void decript(byte[] dt)
            {
                data = dt;
                decript();
            }
        /// <summary>
        /// I know this decrypts 4 bytes at a time using a floating xor key
        /// </summary>
        /// <returns></returns>
            public byte[] decript()
            {
            /* we already know the xor key from packet 0x19 by now.
             * data currently holds the value of packet 0x19 starting with the first 
             * 4 encrypted bytes that is really 0x19, 0x00, 0x00, 0x00 encrypted with a xor key
             */
                int leng = data.Length;
                int pos = 0;
            // we do everything in chunks of 4 bytes
                leng = leng / 4;

            // Converts packet into a int array 4 bytes per single int entry
                Convertbytetoword(leng);

                crc = worddata[0]; // saying crc is the first 4 bytes, when it really is the xor key we grab from 0x19 

            // if we are not supposed to skip these bytes then we multiply the xor key we got by 0x180EF, then add 1
            // skip is always true to begin with
            if (!skip)
                {
                    muliplier *= m2;
                    muliplier++;
                }

                skip = false;
            /* grabs wordata 0 pos = 0 at this point xors it with current multiplier value
             * this is 0x19, 0x00, 0x00, 0x00 but encrypted with the key (multiplier) here we xor it with that value 
             * so wordata[0] always equals 0x19, 0x00, 0x00, 0x00 when unenrypted.
             */
            worddata[pos] ^= muliplier; 
            // increase multiplier by * 0x180EF again
                muliplier *= m2;
            // advance multiplier by one
                muliplier++;

                worddata[pos + 1] ^= muliplier;
                crc ^= worddata[pos + 1];
                muliplier *= m2;
                muliplier++;

                worddata[pos + 2] ^= muliplier;
                crc ^= worddata[pos + 2];
                muliplier *= m2;
                muliplier++;

                worddata[pos + 3] ^= muliplier;
                crc ^= worddata[pos + 3];

                if (leng > 4)
                {
                    pos += 4;
                    leng -= 5;
                }

                for (; leng > 0; pos++, leng--)
                {
                    crc ^= worddata[pos];
                }
                // turn it all back into bytes
                wordtobyte();
                return data;
            }
        /// <summary>
        /// Converts the contents of byte array data to int array worddata in chunks of 4 bytes
        /// </summary>
        /// <param name="l"></param>
            public void Convertbytetoword(int l)
            {
                worddata = new int[l];
                for (int i = 0; i < l; i++)
                {
                    worddata[i] = BitConverter.ToInt32(data, i * 4);
                }
            }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="id">0x19 is the only valid value here as we pull the first nown xor key from known packet 0x19.</param>
            public void decript(byte[] b, int id)
            {
                data = b;
                Int32 ipdat = BitConverter.ToInt32(data, 0);
            // xor key from packet 0x19
                muliplier = ipdat ^ id;
                decript();

            }
        /// <summary>
        /// Copies the contents of the int array worddata to the byte array data
        /// </summary>
            public void wordtobyte()
            {

                for (int i = 0; i < worddata.Length; i++)
                    Array.Copy(BitConverter.GetBytes(worddata[i]), 0, data, i * 4, 4);
            }
        }
    
}
