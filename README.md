# The Realm Offline

### Setup - Prerequisite Software
1. Install Visual Studio Community Edition
1. Install Mysql

### Setup - Database
1. Open the MySql console and create a new database (e.g. realmoffline). Make sure you keep track of the name
1. Populate this database through MySql by importing `realmemu.sql`
1. Create a user for this database. Ensure you give full access to the user when creating it. Keep track of the username/password

### Setup Continued - Server Configuration
1. In RealmOffline/RealmOfflineRouter folder, Copy `Config.xml.sample` as `Config.xml`
1. Update the config so that it points to your Database name. Also, update the username and password to match the user you created in MySql.


### Setup Continued - Building the Code
1. Open RealmOffline.sln with Visual Studio
1. From the top menu, Build -> Rebuild Solution
1. Open the ROLLauncher.sln with Visual Studio
1. From the top menu, Build -> Rebuild Solution
1. There should now be several *.exe files produced in the bin/ folder

Congratulations. You are now a l33t programmer.

### Setup Continued - Running the Emulator
1. Run `RealmOffline/Built/Debug/RealmOfflineRouter.exe`
1. Run `RealmOffline/Built/Debug/RealmOfflineWorldServer.exe`
1. Run `ROLLauncer/Built/Debug/ROLLauncher.exe`
1. When prompted by the ROLLauncher, select the directory where your realm client is installed and then launch!

*note: the *.exe files may be in the `Built/Release` folder if you built in release mode*

## FAQ
### How do I log in?

The emu is set up to create an account for the username/password if one doesn't exist.

That means the first time you boot up, you can enter whatever username/password you want and it will log you in!

### I'm having problems, how do I get help?
Ask for help on our [discord channel](https://discord.gg/nfHUcJ4)

### How can I help?
This project won't happen without the contribution of the community. If you are a developer, join us in the discord channel and feel free to make any pull request to the repo!

### What commands can I use?
TODO: Fill this out
