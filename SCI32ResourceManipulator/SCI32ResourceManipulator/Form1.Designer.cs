﻿namespace SCI32ResourceManipulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openBMPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.checkSizesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paletteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.extractPaletteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.swapPaletteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.picBoxOriginal = new System.Windows.Forms.PictureBox();
            this.picBoxSCI32 = new System.Windows.Forms.PictureBox();
            this.imageInformationControl2 = new SCI32Lib.Controls.ImageInformationControl();
            this.imageInformationControl1 = new SCI32Lib.Controls.ImageInformationControl();
            this.paletteSCI32 = new SCI32Lib.Controls.PaletteControl();
            this.paletteOriginal = new SCI32Lib.Controls.PaletteControl();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxOriginal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSCI32)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.openBMPToolStripMenuItem,
            this.paletteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1205, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.exitToolStripMenuItem.Text = "&Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // openBMPToolStripMenuItem
            // 
            this.openBMPToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem1,
            this.checkSizesToolStripMenuItem});
            this.openBMPToolStripMenuItem.Name = "openBMPToolStripMenuItem";
            this.openBMPToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.openBMPToolStripMenuItem.Text = "OpenBMP";
            // 
            // openToolStripMenuItem1
            // 
            this.openToolStripMenuItem1.Name = "openToolStripMenuItem1";
            this.openToolStripMenuItem1.Size = new System.Drawing.Size(135, 22);
            this.openToolStripMenuItem1.Text = "Open";
            this.openToolStripMenuItem1.Click += new System.EventHandler(this.openToolStripMenuItem1_Click);
            // 
            // checkSizesToolStripMenuItem
            // 
            this.checkSizesToolStripMenuItem.Name = "checkSizesToolStripMenuItem";
            this.checkSizesToolStripMenuItem.Size = new System.Drawing.Size(135, 22);
            this.checkSizesToolStripMenuItem.Text = "Check Sizes";
            this.checkSizesToolStripMenuItem.Click += new System.EventHandler(this.checkSizesToolStripMenuItem_Click);
            // 
            // paletteToolStripMenuItem
            // 
            this.paletteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.extractPaletteToolStripMenuItem,
            this.swapPaletteToolStripMenuItem});
            this.paletteToolStripMenuItem.Name = "paletteToolStripMenuItem";
            this.paletteToolStripMenuItem.Size = new System.Drawing.Size(55, 20);
            this.paletteToolStripMenuItem.Text = "Palette";
            // 
            // extractPaletteToolStripMenuItem
            // 
            this.extractPaletteToolStripMenuItem.Name = "extractPaletteToolStripMenuItem";
            this.extractPaletteToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.extractPaletteToolStripMenuItem.Text = "Extract Palette";
            this.extractPaletteToolStripMenuItem.Click += new System.EventHandler(this.extractPaletteToolStripMenuItem_Click);
            // 
            // swapPaletteToolStripMenuItem
            // 
            this.swapPaletteToolStripMenuItem.Name = "swapPaletteToolStripMenuItem";
            this.swapPaletteToolStripMenuItem.Size = new System.Drawing.Size(148, 22);
            this.swapPaletteToolStripMenuItem.Text = "Swap Palette";
            this.swapPaletteToolStripMenuItem.Click += new System.EventHandler(this.swapPaletteToolStripMenuItem_Click);
            // 
            // picBoxOriginal
            // 
            this.picBoxOriginal.Location = new System.Drawing.Point(658, 27);
            this.picBoxOriginal.Name = "picBoxOriginal";
            this.picBoxOriginal.Size = new System.Drawing.Size(535, 345);
            this.picBoxOriginal.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxOriginal.TabIndex = 1;
            this.picBoxOriginal.TabStop = false;
            this.picBoxOriginal.Click += new System.EventHandler(this.picBoxOriginal_Click);
            // 
            // picBoxSCI32
            // 
            this.picBoxSCI32.Location = new System.Drawing.Point(658, 378);
            this.picBoxSCI32.Name = "picBoxSCI32";
            this.picBoxSCI32.Size = new System.Drawing.Size(535, 314);
            this.picBoxSCI32.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxSCI32.TabIndex = 2;
            this.picBoxSCI32.TabStop = false;
            this.picBoxSCI32.Click += new System.EventHandler(this.picBoxSCI32_Click);
            // 
            // imageInformationControl2
            // 
            this.imageInformationControl2.Location = new System.Drawing.Point(12, 403);
            this.imageInformationControl2.Name = "imageInformationControl2";
            this.imageInformationControl2.Size = new System.Drawing.Size(292, 253);
            this.imageInformationControl2.TabIndex = 6;
            // 
            // imageInformationControl1
            // 
            this.imageInformationControl1.Location = new System.Drawing.Point(15, 43);
            this.imageInformationControl1.Name = "imageInformationControl1";
            this.imageInformationControl1.Size = new System.Drawing.Size(292, 253);
            this.imageInformationControl1.TabIndex = 5;
            // 
            // paletteSCI32
            // 
            this.paletteSCI32.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 2F, System.Drawing.FontStyle.Bold);
            this.paletteSCI32.FontSize = 2F;
            this.paletteSCI32.Location = new System.Drawing.Point(313, 378);
            this.paletteSCI32.Name = "paletteSCI32";
            this.paletteSCI32.PaletteHeight = 269;
            this.paletteSCI32.PaletteRects = ((System.Collections.Generic.Dictionary<System.Drawing.Rectangle, System.Drawing.Color>)(resources.GetObject("paletteSCI32.PaletteRects")));
            this.paletteSCI32.PaletteWidth = 379;
            this.paletteSCI32.Size = new System.Drawing.Size(307, 314);
            this.paletteSCI32.TabIndex = 4;
            this.paletteSCI32.Load += new System.EventHandler(this.paletteSCI32_Load);
            // 
            // paletteOriginal
            // 
            this.paletteOriginal.CellFont = new System.Drawing.Font("Microsoft Sans Serif", 2F, System.Drawing.FontStyle.Bold);
            this.paletteOriginal.FontSize = 2F;
            this.paletteOriginal.Location = new System.Drawing.Point(313, 43);
            this.paletteOriginal.Name = "paletteOriginal";
            this.paletteOriginal.PaletteHeight = 269;
            this.paletteOriginal.PaletteRects = ((System.Collections.Generic.Dictionary<System.Drawing.Rectangle, System.Drawing.Color>)(resources.GetObject("paletteOriginal.PaletteRects")));
            this.paletteOriginal.PaletteWidth = 379;
            this.paletteOriginal.Size = new System.Drawing.Size(307, 304);
            this.paletteOriginal.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1205, 745);
            this.Controls.Add(this.imageInformationControl2);
            this.Controls.Add(this.imageInformationControl1);
            this.Controls.Add(this.paletteSCI32);
            this.Controls.Add(this.paletteOriginal);
            this.Controls.Add(this.picBoxSCI32);
            this.Controls.Add(this.picBoxOriginal);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SCI Resource Manipulator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxOriginal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxSCI32)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.PictureBox picBoxOriginal;
        private System.Windows.Forms.PictureBox picBoxSCI32;
        private SCI32Lib.Controls.PaletteControl paletteOriginal;
        private SCI32Lib.Controls.PaletteControl paletteSCI32;
        private SCI32Lib.Controls.ImageInformationControl imageInformationControl1;
        private SCI32Lib.Controls.ImageInformationControl imageInformationControl2;
        private System.Windows.Forms.ToolStripMenuItem openBMPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem checkSizesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paletteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem extractPaletteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem swapPaletteToolStripMenuItem;
    }
}

